# REST DNS

Download the zip file and extract 

Generate the Eclipse project files

```
  # cd to $PROJ_HOME/rest-dns
  # ./gradlew cleanEclipse eclipse
```

```
  # cd $PROJ_HOME/rest-dns
  # ./gradlew clean build
```

Invoke the main program of "com.rest.dns.Start" class. It will bring up web server

PUT - http://localhost:8080/dns

Content-Type : application/json

Body : {"www.google.com":["10.132.45.45","10.45.34.67"]}

To do registration


GET - http://localhost:8080/dns

To get the IP resolution in round robin way !!
