package com.rest.dns.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rest.dns.service.DomainDnsRegistryService;

@RestController
@RequestMapping("/dns")
public class DomainDnsRegistryController {

    @Autowired DomainDnsRegistryService domainDnsRegistryService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean create(@RequestBody Map<String, List<String>> input) {

        if (input != null && !input.isEmpty()) {
            input.forEach((name, mappings) -> {
                domainDnsRegistryService.addRecord(name, mappings);
            });
            return true;
        } else {
            return false;
        }

    }

    @RequestMapping(value = "/{name}")
    public String read(@PathVariable String name) {
        if (name != null && !name.isEmpty()) {
            return domainDnsRegistryService.getRecord(name);
        } else {
            return "Please provide name";
        }
    }

}
