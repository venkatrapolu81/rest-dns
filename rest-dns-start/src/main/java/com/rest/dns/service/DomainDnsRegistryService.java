package com.rest.dns.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

@Service
public class DomainDnsRegistryService {

    private final Map<String, List<String>> registryMappings = new ConcurrentHashMap<>();

    private final Map<String, Integer> registryMappingsPointer = new ConcurrentHashMap<>();

    public void addRecord(String name, List<String> values) {
        if (name != null && !name.isEmpty() && values != null && !values.isEmpty()) {
            registryMappings.put(name, values);
            registryMappingsPointer.put(name, -1);
        } else {
            throw new RuntimeException("name, values can not be null/empty");
        }
    }

    public String getRecord(String name) {
        if (name != null && !name.isEmpty()) {
            int pointer = registryMappingsPointer.get(name) + 1;
            if (pointer >= registryMappings.get(name).size()) {
                pointer = 0;
            }
            registryMappingsPointer.put(name, pointer);
            String result = registryMappings.get(name).get(pointer);
            return result;
        } else {
            throw new RuntimeException("name can not be null/empty");
        }
    }

    public List<String> getRecords(String name) {
        if (name != null && !name.isEmpty()) {
            return registryMappings.get(name);
        } else {
            throw new RuntimeException("name can not be null/empty");
        }
    }

}
