package com.rest.dns.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSON {
    private static Logger LOG = LoggerFactory.getLogger(JSON.class);
    private final static ObjectMapper mapper = new ObjectMapper();

    public static String string(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert bean to JSON String", e);
        }
    }

    public static <T> T object(String json, Class<T> valueType) {
        try {
            return mapper.readValue(json, valueType);
        } catch (Exception e) {
            LOG.error("Exception thrown ", e);
            throw new RuntimeException("Failed to convert JSON String to Bean", e);
        }
    }

    public static <T> T object(Path path, Class<T> clazz) {
        try (BufferedReader br = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            return mapper.readValue(br, clazz);
        } catch (Exception e) {
            throw new RuntimeException("Failed to decode JSON", e);
        }
    }

    public static <T> T object(Reader reader, Class<T> clazz) {
        try {
            return mapper.readValue(reader, clazz);
        } catch (IOException e) {
            throw new RuntimeException("Failed to decode JSON", e);
        }
    }

    public static boolean isValidJson(String template) {
        boolean isValidJson = true;
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.readTree(template);
        } catch (IOException e) {
            isValidJson = false;
        }
        return isValidJson;
    }

    public static void main(String[] args) {
        Map<String, List<String>> input = new HashMap<>();
        List<String> ips = new ArrayList<>();
        ips.add("10.132.45.45");
        ips.add("10.45.34.67");
        input.put("www.google.com", ips);
        System.out.println(JSON.string(input));
    }

}
