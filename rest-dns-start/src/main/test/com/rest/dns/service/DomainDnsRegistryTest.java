package com.rest.dns.service;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class DomainDnsRegistryTest {

    @Test
    public void testRoundRobinRead() {
        DomainDnsRegistryService registry = new DomainDnsRegistryService();
        registry.addRecord("google.com", Arrays.asList("216.58.196.110", "216.58.196.111", "216.58.196.112", "216.58.196.113"));

        Assert.assertEquals("Wrong ip fetched", "216.58.196.110", registry.getRecord("google.com"));
        Assert.assertEquals("Wrong ip fetched", "216.58.196.111", registry.getRecord("google.com"));
        Assert.assertEquals("Wrong ip fetched", "216.58.196.112", registry.getRecord("google.com"));
        Assert.assertEquals("Wrong ip fetched", "216.58.196.113", registry.getRecord("google.com"));

        Assert.assertEquals("Wrong ip fetched", "216.58.196.110", registry.getRecord("google.com"));
        Assert.assertEquals("Wrong ip fetched", "216.58.196.111", registry.getRecord("google.com"));
        Assert.assertEquals("Wrong ip fetched", "216.58.196.112", registry.getRecord("google.com"));
    }

}
