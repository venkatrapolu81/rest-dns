package com.rest.dns.service.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.rest.dns.controller.DomainDnsRegistryController;
import com.rest.dns.service.DomainDnsRegistryService;

@RunWith(SpringRunner.class)
@WebMvcTest(DomainDnsRegistryController.class)
public class DomainDnsRegistryControllerTest {

    @Autowired private MockMvc mockMvc;

    @MockBean private DomainDnsRegistryService domainRegistryService;

    @Test
    public void performRegistration() throws Exception {
        Mockito.doNothing().when(domainRegistryService).addRecord(Mockito.anyObject(), Mockito.anyObject());
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/dns").accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON).content("{\"www.google.com\":[\"10.132.45.45\",\"10.45.34.67\"]}");

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void performRegistrationGet() throws Exception {
        Mockito.when(domainRegistryService.getRecord(Mockito.anyString())).thenReturn("127.0.0.1");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/dns/www.google.com");

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals("127.0.0.1", response.getContentAsString());

    }

}
